/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
SELECT o1.orderNumber, SUM(priceEach*quantityOrdered) totalPrice
FROM orders o1
INNER JOIN orderdetails o2 ON o1.orderNumber = o2.orderNumber
GROUP BY orderNumber
HAVING totalPrice > 60000